function HelloWorld ({ frase, upper, lower, reverse, reverseUpper, reverseLower, shuffled, shuffledUpper, shuffledLower, random, data, hora }) {
  return (
    <div>
      <h2>{frase}, {data}, {hora}</h2>
      <h2>{upper}, {data}, {hora}</h2>
      <h2>{lower}, {data}, {hora}</h2>
      <h2>{reverse}, {data}, {hora}</h2>
      <h2>{reverseUpper}, {data}, {hora}</h2>
      <h2>{reverseLower}, {data}, {hora}</h2>
      <h2>{shuffled}, {data}, {hora}</h2>
      <h2>{shuffledUpper}, {data}, {hora}</h2>
      <h2>{shuffledLower}, {data}, {hora}</h2>
      <h2>{random}, {data}, {hora}</h2>
    </div>
  )
}

export default HelloWorld;