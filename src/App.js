import HelloWorld from './components/HelloWorld';
import './App.css';

function App() {

  const frase = 'Hello World'
  const upper = frase.toUpperCase();
  const lower = frase.toLowerCase();
  const reverse = frase.split("").reverse().join("");
  const reverseUpper = upper.split("").reverse().join("");
  const reverseLower = lower.split("").reverse().join("");
  const shuffled = frase.split("").sort().join('');
  const shuffledUpper = upper.split("").sort().join('');
  const shuffledLower = lower.split("").sort().join('');
  const random = frase.split('').sort(function(){return 0.5-Math.random()}).join('');

  let hoje = new Date();
  let data = hoje.getDate() + '/' + ( hoje.getMonth() + 1 ) + '/' + hoje.getFullYear();
  let hora = hoje.getHours() + ':' + hoje.getMinutes() + ':' + hoje.getSeconds();

  return (
    <div className="App">
      <HelloWorld 
        frase={frase}
        upper={upper}
        lower={lower}
        reverse={reverse}
        reverseUpper={reverseUpper}
        reverseLower={reverseLower}
        shuffled={shuffled}
        shuffledUpper={shuffledUpper}
        shuffledLower={shuffledLower}
        random={random}
        data={data}
        hora={hora}
      />
    </div>
  );
}

export default App;
